﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1071638.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Html()
        {
            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }

        public ActionResult Razor()
        {
            return View();
        }

        public ActionResult Viewdata()
        {
            ViewData["message"] = "訊息";
            return View();
        }

        public ActionResult Viewbag()
        {
            ViewBag.message = "訊息";
            return View();
        }

    }
}